type t = { mots : string list ; branches : (int * t) list }

(* Question 1 *)
let empty = { mots = []; branches = []}

(* Question 2 *)
let find l a =
    let rec aux l a =
        match l with
        | [] -> a.mots
        | x::s -> aux s (List.assoc x a.branches)
    in try aux l a
    with Not_found -> []

let get a =
    let rec aux t acc =
        List.fold_left (fun acc (_,t) -> aux t acc)
        (List.rev_append t.mots acc)
        t.branches
    in aux a []


(* Question 3 *)
let change_assoc x v l =
    let rec aux x v l =
        match l with
        | [] -> [(x,v)]
        | (a,b) :: s ->
            if (a = x) then
                (x,v) :: s
            else
                (a,b) :: (aux x v s)
    in
    aux x v l

let print_assoc_list t =
    List.iter (fun (a,b) -> Printf.printf "(%d,%d)-" a b) t

(* Question 4 *)
let add l m a =
    let rec aux l m a =
        match l with 
        | [] -> if (List.exists (fun x -> x = m) a.mots) then
                    a
                else
                    {mots = m :: a.mots; branches = a.branches}

        | x :: s -> {
                        mots = a.mots;
                        branches = change_assoc x (
                            aux s m (
                                try List.assoc x a.branches
                                with Not_found -> empty
                            )
                        ) a.branches
                    } 
    in aux l m a