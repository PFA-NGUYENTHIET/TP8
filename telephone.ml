(* Question 7 *)
open T9
open Clavier

let touche_vers_int t =
    match t with
    | 'a' | 'b' | 'c' | '-' -> 2
    | 'd' | 'e' | 'f' -> 3
    | 'g' | 'h' | 'i' -> 4
    | 'j' | 'k' | 'l' -> 5
    | 'm' | 'n' | 'o' -> 6
    | 'p' | 'q' | 'r' | 's' -> 7
    | 't' | 'u' | 'v' -> 8
    | 'w' | 'x' | 'y' | 'z' -> 9
    | _ -> 1

let intlist_of_string s =
    let rec aux i n s acc =
        if i = n then
            acc
        else
            aux (i+1) n s (touche_vers_int(s.[i]) :: acc)
    in List.rev (aux 0 (String.length(s)) s [])

(* Question 8 *)
let create_tree flux =
    let rec aux flux acc =
        try 
            let str = input_line flux in
            aux flux (add (intlist_of_string str) str acc) 
        with End_of_file ->
            acc
    in
    aux flux empty

let rec boucle l a =
    let next = clic () in
    let new_l = next :: l in
    let str_list = find (List.rev new_l) a in
    List.iter (Printf.printf "%s ") str_list;
    print_newline ();
    boucle new_l a

let () =
    let flux = open_in "./dico.txt" in
    let arbre = create_tree flux in
    show ();
    boucle [] arbre
