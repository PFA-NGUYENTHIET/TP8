.PRECIOUS: %.cmi %.cmo

# all:
# 	ocamlbuild -use-ocamlfind -package graphics telephone

# clean:
# 	ocamlbuild -clean
clavier : clavier.cmo 
	ocamlc -o ./a.out graphics.cma clavier.cmo 

telephone : clavier.cmo t9.cmo telephone.cmo 
	ocamlc -o ./a.out graphics.cma clavier.cmo t9.cmo telephone.cmo

run :
	make telephone;
	ocamlrun ./a.out

%.cmi : %.mli
	ocamlc -c $^

%.cmo : %.cmi %.ml
	ocamlc -c $(word 2,$^)

clean :
	rm -rf *.cm*
	rm -rf a.out