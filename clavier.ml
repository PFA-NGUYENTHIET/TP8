(* Question 5 *)
open Graphics

let coeff = 100

let draw_grid length width =
    let foo a b c d =
        moveto a b;
        lineto c d;
    in

    let rec bar a b n =
    if n > 0 then
        let tmp = (n*coeff) in
        foo tmp a tmp b;
        bar a b (n-1)
    in
    bar 0 length 2;

    let rec aux a b n =
    if n > 0 then
        let tmp = (n*coeff) in
        foo a tmp b tmp;
        aux a b (n-1)
    in
    aux 0 width 3

let put_numbers length width =
    let aux s x y =
        moveto x y;
        draw_string s
    in

    let foo s x y = 
        aux s (coeff*x*3/9) (coeff*y*4/8)
    in
    foo "1" 1 7;
    foo "2 abc" 4 7;
    foo "3 def" 7 7;
    foo "4 ghi"  1 5;
    foo "5 jkl"  4 5;
    foo "6 mno"  7 5;
    foo "7 pqrs"  1 3;
    foo "8 tuv"  4 3;
    foo "9 wxyz"  7 3;
    foo "0" 4 1

let show () =
    let length = 4 * coeff in
    let width = 3 * coeff in

    open_graph (
        " " ^(
            string_of_int width
        ) ^ "x" ^ (
            string_of_int length
        )
    );
    draw_grid length width;
    put_numbers length width

let clic () = 
    let status = wait_next_event [Button_down] in
    let x = status.mouse_x in
    let y = status.mouse_y in

    if y < 1*coeff then
        if x < 1*coeff then
            -1
        else if x < 2*coeff then
            0
        else
            -1
    else if y < 2*coeff then
        if x < 1*coeff then
            7
        else if x < 2*coeff then
            8
        else
            9
    else if y < 3*coeff then
        if x < 1*coeff then
            4
        else if x < 2*coeff then
            5
        else
            6
    else
        if x < 1*coeff then
            1
        else if x < 2*coeff then
            2
        else
            3